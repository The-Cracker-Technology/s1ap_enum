#!/usr/bin/env escript
%% -*- erlang -*-
%%! -noshell -sname s1ap -smp auto

%  s1ap.erl
%  
%  Copyright 2014 Daniel Mende <mail@c0decafe.de>
%  
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
%  
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%  
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, write to the Free Software
%  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
%  MA 02110-1301, USA.


%~ Compile ASN.1 to erl:
%~ 
%~ erlc -bper S1AP.set.asn1

%~ Open erl shell:
%~ 
%~ c(s1ap).
%~ rd(eNBid, { mcc, mnc, id, name }).
%~ s1ap:setup("127.0.0.1", #eNBid{mcc="\x20\xf1",mnc="\x01",id="\x32\x12",name="test123"}).

-module(s1ap).
-compile([nowarn_unused_function, nowarn_unused_vars]).
-include("$PWD/S1AP.hrl").
-include_lib("kernel/include/inet_sctp.hrl").
-export([   enum_enbid/2,
            brute_enbid/2
        ]).

-define(S1AP_PORT, 36412).
-define(S1AP_BIND, 36412).
-define(S1AP_PPID, 18).
-define(SCTP_STREAM, 0).

-define(CONNECT_TIMEOUT, 10000).
-define(RECV_TIMEOUT, 10000).
-define(SOCK_ERR_DELAY, 100).
-define(SOCK_CONNECT_RETRY, 10).

-record(eNBid, { mcc, mnc, id, name }).

create_id(Mcc, Mnc, Id, Name) ->
    {First, Second} = lists:split(2, integer_to_list(Mcc)),
    MccB = hex:hexstr_to_bin(lists:append([lists:reverse(First), "f", Second])),
    List = integer_to_list(Mnc),
    if 
    length(List) < 2 -> Tmp = lists:append(["0", List]);
    true ->             Tmp = integer_to_list(Mnc)
    end,
    {Tmp2, _} = lists:split(2, Tmp),
    MncB = hex:hexstr_to_bin(lists:reverse(Tmp2)),
    #eNBid{mcc=binary_to_list(MccB),mnc=binary_to_list(MncB),id=Id,name=Name}.

setup_request_message(Id) ->
    {initiatingMessage,
        {'InitiatingMessage',17,reject,
            {'S1SetupRequest',
                [{'ProtocolIE-Field',59,reject,
                     {'Global-ENB-ID',lists:append(Id#eNBid.mcc, Id#eNBid.mnc),
                         {'macroENB-ID',[0,0,0,0, 0,0,0,0, 0,0,0,1, 1,0,1,0, 0,1,1,1]},
                         asn1_NOVALUE}},
                 {'ProtocolIE-Field',60,ignore,Id#eNBid.name},
                 {'ProtocolIE-Field',64,reject,
                     [{'SupportedTAs-Item',[0,123],[lists:append(Id#eNBid.mcc,Id#eNBid.mnc)],asn1_NOVALUE}]},
                 {'ProtocolIE-Field',137,ignore,v256}]}}}.

%~ initial_ue_message(Id) ->
    %~ {initiatingMessage,
        %~ {'InitiatingMessage',12,ignore,
            %~ {'InitialUEMessage',
                %~ [{'ProtocolIE-Field',8,reject, {'id-eNB-UE-S1AP-ID', 1}},
                 %~ {'ProtocolIE-Field',26,reject, {'id-NAS-PDU',
                    %~ {'NAS-PDU', 
                    %~ }}},
                 %~ {'ProtocolIE-Field',67,reject, {'id-TAI',
                    %~ {'TAIItem',lists:append(Id2#eNBid.mcc,Id2#eNBid.mnc),[0,123],asn1_NOVALUE}}},
                 %~ {'ProtocolIE-Field',100,reject, {'id-EUTRAN-CGI',
                    %~ }},
                 %~ {'ProtocolIE-Field',134,reject, {'id-RRC-Establishment-Cause',
                    %~ }}
                %~ ]}}}.

check_error(Msg, Verbose) ->
    Return = case Msg of
    {unsuccessfulOutcome,
     {'UnsuccessfulOutcome',17,reject, Failure}} ->
        if
        Verbose == true ->
            case Failure of
            {_, PIEs} ->
                case lists:nth(1, PIEs) of
                {'ProtocolIE-Field',2,ignore,{misc,Reason}} ->
                    io:fwrite("Reason: ~s\n", [Reason]);
                    true -> nop
                end
            end;
        true -> nop
        end,
        error;
    _ -> ok
    end,
    Return.
    
open_sock(Address, Verbose) ->
    Return = case inet_parse:address(Address) of
    {ok, Addr} ->
        if 
        Verbose == true -> io:fwrite("Addr: ~w\n", [Addr]);
        true -> nop
        end,
        Return1 = case gen_sctp:open([{ip, any}, {port, ?S1AP_BIND }, {type, seqpacket}, {reuseaddr, true}]) of
        {ok, Sock} ->
            if 
            Verbose == true -> io:fwrite("Sock: ~w\n", [Sock]);
            true -> nop
            end,
            Return2 = case gen_sctp:connect(Sock, Addr, ?S1AP_PORT, [], ?CONNECT_TIMEOUT) of
            {ok, Assoc} ->
                if 
                Verbose == true -> io:fwrite("Assoc: ~w\n", [Assoc]);
                true -> nop
                end,
                {ok, {Sock, Assoc}};
            {error, eaddrnotavail} ->
                timer:sleep(?SOCK_ERR_DELAY),
                %~ io:fwrite("Can't connect: ~s!\n", [eaddrnotavail]),
                {error, Sock};
            {error, Reason} ->
                timer:sleep(?SOCK_ERR_DELAY),
                %~ io:fwrite("Can't connect: ~s!\n", [Reason]),
                {error, Sock}
           end,
           Return2;
        {error, Reason} ->
            timer:sleep(?SOCK_ERR_DELAY),
            %~ io:fwrite("Can't create socket: ~s!\n", [Reason]),
            {error}
        end,
        Return1;
    {error, einval} ->
        io:fwrite("Can't parse address!\n"),
        {error}
    end,
    Return.

try_open_sock(Address, Verbose, 0, _) ->
    open_sock(Address, Verbose);
try_open_sock(Address, Verbose, RetriesLeft, Delay) ->
    case open_sock(Address, Verbose) of
    {ok, {Sock, Assoc}} ->
        {ok, {Sock, Assoc}};
    {error, Sock} ->
        close_sock(Sock),
        io:fwrite("."),
        timer:sleep(?SOCK_ERR_DELAY),
        try_open_sock(Address, Verbose, RetriesLeft - 1, Delay * 2);
    {error} ->
        timer:sleep(?SOCK_ERR_DELAY),
        io:fwrite("."),
        try_open_sock(Address, Verbose, RetriesLeft - 1, Delay * 2)
    end.

close_sock(Sock) ->
    gen_sctp:close(Sock).

recv_pdu(Sock, Verbose) ->
    Return = case gen_sctp:recv(Sock, ?RECV_TIMEOUT) of
    {ok, {FromIP, FromPort, [], _}} ->
        recv_pdu(Sock, Verbose);
    {ok, {FromIP, FromPort, AncData, Data2}} ->
        if
        Verbose == true -> io:fwrite("FromIP: ~w, FromPort: ~w, AncData: ~w, Data: ~w\n", [FromIP, FromPort, AncData, Data2]);
        true -> nop
        end,
        {ok, Decode} = 'S1AP':decode('S1AP-PDU', binary_to_list(Data2)),
        if
        Verbose == true -> io:fwrite("From ~w:~w\nReceived: ~w\n", [FromIP, FromPort, Decode]);
        true -> nop
        end,
        {check_error(Decode, Verbose), Decode};
    {error, Reason} ->
        io:fwrite("Can't recv: ~w!\n", [Reason]),
        {error}
    end,
    Return.

send_pdu({Sock, Assoc}, Pdu, Verbose) ->
    {ok, Data} = 'S1AP':encode('S1AP-PDU', Pdu),
    if 
    Verbose == true -> io:fwrite("Data: ~w\n", [Data]);
    true -> nop
    end,
    Info = #sctp_sndrcvinfo{ stream = ?SCTP_STREAM, ppid = ?S1AP_PPID, assoc_id = Assoc#sctp_assoc_change.assoc_id },
    Return = case gen_sctp:send(Sock, Info, Data) of
    {error, Reason} ->
        io:fwrite("Can't send: ~s!\n", [Reason]),
        {error};
    _ ->
        recv_pdu(Sock, Verbose)
    end,
    Return.

enum_enbid(Address, Verbose) ->
    List = mcc_list:list(),
    %io:fwrite("~w\n", [List]),
    lists:foreach(fun(X) -> 
        Return = case try_open_sock(Address, Verbose, ?SOCK_CONNECT_RETRY, ?SOCK_CONNECT_RETRY) of
        {ok, {Sock, Assoc}} ->
            %~ io:fwrite("Sock: ~w, Assoc: ~w\n", [Sock, Assoc]),
            {Mcc, Mnc, Network, Name, Status} = X,
            Id = create_id(Mcc, Mnc, "\x32\x12", "eNB1"),
            io:fwrite("Trying PLMN-ID: ~w\n", [Id]),
            Return2 = case send_pdu({Sock, Assoc}, setup_request_message(Id), Verbose) of
                {ok, Decode} ->
                    io:fwrite("\e[92;1mGot good answer for Mcc ~w, Mnc ~w\n~w\n\e[39;49;0m", [Mcc, Mnc, Decode]),
                    io:fwrite("\e[93;1mNetwork: ~s, Name ~s\n\e[39;49;0m", [Network, Name]),
                    gather_info(Sock, Assoc, Mcc, Mnc, Verbose),
                    {ok, X, Decode};
                {error, Decode} ->
                    io:fwrite("\e[91;1mGot bad answerfor Mcc ~w, Mnc ~w\n~w\n\e[39;49;0m", [Mcc, Mnc, Decode]),
                    {error, X, Decode};
                _ ->
                    io:fwrite("!"),
                    {error, X}
                end,
            close_sock(Sock),
            Return2;
        {error, Sock} ->
            close_sock(Sock),
            io:fwrite("!"),
            {error, ""};
        {error} ->
            {error, ""},
            io:fwrite("!")
        end,
        Return
     end, List).

brute_enbid(Address, Verbose) ->
    Mccs = lists:seq(100,999),
    Mncs = lists:seq(1,20),
    lists:foreach(fun(X) ->
        lists:foreach(fun(Y) ->
           case try_open_sock(Address, Verbose, ?SOCK_CONNECT_RETRY, ?SOCK_CONNECT_RETRY) of
            {ok, {Sock, Assoc}} ->
                Id = create_id(X, Y, "\x32\x12", "eNB1"),
                io:fwrite("Trying PLMN-ID: ~w\n", [Id]),
                Return2 = case send_pdu({Sock, Assoc}, setup_request_message(Id), Verbose) of
                    {ok, Decode} ->
                        io:format("\e[92;1mGot good answer for Mcc ~w, Mnc ~w\n~w\n\e[39;49;0m", [X, Y, Decode]),
                        gather_info(Sock, Assoc, X, Y, Verbose),
                        {ok, Id, Decode};
                    {error, Decode} ->
                        io:format("\e[91;1mGot bad answer for Mcc ~w, Mnc ~w\n~w\n\e[39;49;0m", [X, Y, Decode]),
                        {error, Id, Decode};
                    _ ->
                        {error, Id}
                    end,
                close_sock(Sock),
                Return2;
            {error, Sock} ->
                close_sock(Sock),
                io:fwrite("!"),
                {error, ""};
            {error} ->
                io:fwrite("!"),
                {error, ""}
            end
        end, Mncs)
    end, Mccs ).


gather_info(Address, Mcc, Mnc, Verbose) ->
    Return = case inet_parse:address(Address) of
    {ok, Addr} ->
        Return1 = case try_open_sock(Address, Verbose, ?SOCK_CONNECT_RETRY, ?SOCK_CONNECT_RETRY) of
        {ok, {Sock, Assoc}} ->
            Id = create_id(Mcc, Mnc, "\x32\x12", "eNB1"),
            Return2 = case send_pdu({Sock, Assoc}, setup_request_message(Id), Verbose) of
                {ok, Decode} ->
                    io:fwrite("\e[92;1mGot good answer for Mcc ~w, Mnc ~w\n~w\n\e[39;49;0m", [Mcc, Mnc, Decode]),
                    gather_info(Sock, Assoc, Mcc, Mnc, Verbose),
                    {ok, Decode};
                {error, Decode} ->
                    io:fwrite("\e[91;1mGot bad answerfor Mcc ~w, Mnc ~w\n~w\n\e[39;49;0m", [Mcc, Mnc, Decode]),
                    {error, Decode};
                _ ->
                    io:fwrite("!"),
                    {error}
                end,
            close_sock(Sock),
            Return2;
        {error, Sock} ->
            close_sock(Sock),
            io:fwrite("!"),
            {error, ""};
        {error} ->
            io:fwrite("!"),
            {error, ""}
        end,
        Return1;
    {error, einval} ->
        io:fwrite("Can't parse address!\n"),
        {error}
    end,
    Return.

gather_info(Sock, Assoc, Mcc, Mnc, Verbose) ->
    {ok}.

main(["enum", String]) ->
    try
        enum_enbid(String, false)
    catch
        _:X ->
            io:fwrite("~w\n", [X]),
            usage()
    end;
main(["brute", String]) ->
    try
        brute_enbid(String, false)
    catch
        _:X ->
            io:fwrite("~w\n", [X]),
            usage()
    end;
main(["info", String, Mcc, Mnc]) ->
    try
        {MccI, _} = string:to_integer(Mcc),
        {MncI, _} = string:to_integer(Mnc),
        gather_info(String, MccI, MncI, false)
    catch
        _:X ->
            io:fwrite("~w\n", [X]),
            usage()
    end;
main(_) ->
    usage().

usage() ->
    io:format("usage: s1ap mode target args\n"),
    halt(1).

